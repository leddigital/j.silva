<div class="sidebar-header">
    <div class="sidebar-title">
        Menu Principal
    </div>
    <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
        data-fire-event="sidebar-left-toggle">
        <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>

<div class="nano">
    <div class="nano-content">
        <nav id="menu" class="nav-main" role="navigation">
            <ul class="nav nav-main">
                <li class="nav-active">
                    <a class="nav-link" href="{{ route('information.index') }}">
                        <i class="fas fa-info"></i>
                        <span>Informações Gerais</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('banners.index') }}">
                        <i class="fas fa-images"></i>
                        <span>Banner Slide</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('mainbanner.index') }}">
                        <i class="far fa-star"></i>
                        <span>Banner Interno</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('categories.index') }}">
                        <i class="fas fa-list-ul"></i>
                        <span>Categorias</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('content.index',['_type'=>'sobre']) }}">
                        <i class="far fa-star"></i>
                        <span>Sobre</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('content.index',['_type'=>'projetos']) }}">
                        <i class="far fa-star"></i>
                        <span>Projetos Especiais</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('content.index',['_type'=>'news']) }}">
                        <i class="far fa-newspaper"></i>
                        <span>Blog / Notícias</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('content.index',['_type'=>'midias']) }}">
                        <i class="far fa-newspaper"></i>
                        <span>Mídias</span>
                    </a>
                </li>
            </ul>
        </nav>
        <hr class="separator" />
    </div>
</div>
