@extends('layouts.default')
@section('content')

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Blog</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.blog') }}">Blog</a></li>
                @if (request()->get('search'))
                <li><a href="{{ route('nav.blog') }}">Pesquisando por: {{ request()->get('search') }}</a></li>
                @endif
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================Blog Area =================-->
<section class="main_blog_area">
    <div class="container">
        <div class="row main_blog_inner">
            @if ($posts->total())
            @if (request()->get('search'))
            <div class="search-info">
                <div class="col-md-8 text-info">
                    <h2 class="">Pesquisando por: <span>{{ request()->get('search') }}</span></h2>
                </div>
                <div class="col-md-4 counter-info">
                    <h5><span>{{ count($posts) }}</span>
                        {{ count($posts) > 1 ? 'notícias encontradas' : 'notícia encontrada'  }}</h5>
                </div>
            </div>
            @endif
            @foreach ($posts as $post)
            <div class="col-sm-6">
                <div class="blog_item">
                    <a href="{{ route('nav.post', ['url' => $post->url]) }}" class="blog_img">
                        <img src="{{ asset('content/' . $post->id . "/" . $post->image) }}" alt="">
                    </a>
                    <div class="blog_text">
                        <a href="{{ route('nav.post', ['url' => $post->url]) }}">
                            <h4>{{ $post->title }}</h4>
                        </a>
                        <ul>
                            <li><a href="{{ route('nav.post', ['url' => $post->url]) }}">{{ $post->created_at->format('d/m/Y') }}</a></li>
                        </ul>
                        <p>{{ str_limit($post->short_description, $limit = 100, $end = '...') }}</p>
                        <a class="book_now_btn" href="{{ route('nav.post', ['url' => $post->url]) }}">Leia mais</a>
                    </div>
                </div>
            </div>
            @endforeach



            <div class="row">
                <div class="col-12">
                    <div class="pagination-links text-center">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>

            @else

            <section class="nothing-found text-center">

                <h1>Não encontramos posts com <span>{{ request()->get('search') }}</span></h1>
                <h4>Talvez sua busca tenha sido muito específica. Faça uma nova pesquisa usando termos mais genéricos</h4>

                <aside class="r_widget search_widget">
                    <div class="input-group search-form-not-found">
                        <form action="{{ route('nav.search') }}" method="GET">
                            <span class="input-group-btn">
                                <input name="search" type="text" class="form-control" placeholder="O que você procura?" size="45" required>
                                <button class="btn btn-default" type="submit"><i class="icon icon-Search"></i></button>
                            </span>
                        </form>
                    </div>
                </aside>

                <img width="150px" class="mx-auto"src="{{ asset('images/nothing-found.png') }}" alt="">

            </section>



            @endif
        </div>
    </div>
</section>
<!--================End Blog Area =================-->

@endsection
