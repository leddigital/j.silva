@extends('layouts.default')
@section('content')

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Mídias</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.midias') }}">Mídias</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->





<section class="room_list_area">
    <div class="container">
        <div class="row room_list_inner">

            @foreach ($midias as $midia)

            <div class="room_list_item">
                <div class="col-md-4">
                    <a href="{{ route('nav.midia', [$midia->url]) }}" class="room_img">
                        <img src="/content/{{ $midia->id }}/{{ $midia->image }}" alt="{{ $midia->title }}">
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="room_list_l_text">
                                <a href="{{ route('nav.midia', [$midia->url]) }}"><h4>{{ $midia->title }}</h4></a>
                                <ul>
                                    <li><a href="{{ route('nav.midia', [$midia->url]) }}">info 1</a></li>
                                    <li><a href="{{ route('nav.midia', [$midia->url]) }}">info 2</a></li>
                                    <li><a href="{{ route('nav.midia', [$midia->url]) }}">info 3</a></li>
                                </ul>
                                <p>{!! $midia->short_description !!}</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="room_price_box">

                                <a class="book_now_btn" href="{{ route('nav.midia', [$midia->url]) }}">Ver detalhes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>
</section>












@endsection
