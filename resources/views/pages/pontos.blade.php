@extends('layouts.default')
@section('css')


<script language="JavaScript">
	var clientej = "LWDemo";
	var grupo_padrao = 1; //1//9 -> padrao
	var cidade_padrao = 16495;
	var vnivel = 6; //6 -> razoavel //8 -> maximo //0 -> nenhum
	var AceitaRotaAproximada = true;
	// Mostra rota mesmo se tiverem endereços que não foram encontrados
	var AceitaRotaPontosInexistentes = true;
	var MarcaPontosIntermediariosRota = true;
	var modeloImagem = 's3';
</script>


<link rel="stylesheet" href="{{ asset('css/pontos.css') }}">
<link rel="stylesheet" href="http://newesouth.logycware.com.br/css/estilos_import.css" />
<link rel="stylesheet" href="http://newesouth.logycware.com.br/estilosn.css" />
<link rel="stylesheet" href="http://newesouth.logycware.com.br/estilos.css" />

{{-- <link rel="stylesheet" href="//eSouth.logycware.com/demonstracao/perfil/verde/estilos_perfil.css" /> --}}

@endsection
@section('content')



<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Pontos</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.midias') }}">Pontos</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->



<div class="content" id="pontos">

    {!! $content !!}

</div>



@endsection
@section('js')
<script src="{{ asset('js/pontos.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&key="></script>
<script type="text/javascript" src="//eSouth.logycware.com/demonstracao/includes/mapsV3.js"></script>
<script type="text/javascript" src="//eSouth.logycware.com/demonstracao/includes/httpreq_30.js"></script>






<script language="JavaScript">

    function imprimir(it){
    //	popup( 'impressao_locais.asp',960,600);
        document.pesquisa.action = 'impressao_locais.asp';
        document.pesquisa.target = 'formtarget';
    //	document.pesquisa.onSubmit="popup('about:blank','formtarget','width=300,height=300')";
    ///	document.pesquisa.onsubmixt="";
        popup('about:blank',990,600);
        document.pesquisa.submit();
    };

    function popup(url,wdh,hgt){
        posL = screen.availWidth/2 - wdh/2;
        posT = screen.availHeight/2 - hgt/2;
        str = 'width='+wdh+', height='+hgt+', left='+posL+', top='+posT+', scrollbars=yes';
        window.open(url, 'formtarget', str);
    }

    function pesquisar(){
        if (document.getElementById('div_resultado')){
            document.getElementById('div_resultado').innerHTML = '<table width="100%" class="bordatopocinza" height="5" cellpadding="0" cellspacing="0"><tr><td align="center"><div id="carregandor" align="center"><img border="0" alt="" src="../images/indicator.gif" vspace="0" hspace"3" align="absmiddle"> &nbsp;&nbsp; <font color="#666666" size="1" face="Arial, Helvetica, sans-serif">localizando pontos...</font></div></td></tr></table>';
        }

        document.pesquisa.action = 'javascript:resultado(1);';
        document.pesquisa.target = '';
    //	document.pesquisa.onsubmit= '';
        document.pesquisa.submit();
        document.getElementById('bt_imprimir').style.display='';
        document.getElementById('bt_imprimir2').style.display='';
    }

    function MM_openBrWindow(theURL,winName,features) {
      window.open(theURL,winName,features);
    }


    var local;
        local = "locais";
        produtos();
        setTimeout("pesquisar();", 500);

    </script>

@endsection
