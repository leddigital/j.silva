@extends('layouts.default')
@section('content')

<!--================Banner Area =================-->
<section class="banner_area" style="background: url({{ asset('/mainbanner/'.$informations->main_banner) }}) no-repeat scroll center center;">
    <div class="container">
        <div class="banner_inner_content">
            <h3>Notícias</h3>
            <ul>
                <li class="active"><a href="{{ route('nav.index') }}">Home</a></li>
                <li><a href="{{ route('nav.midias') }}">Pesquisando por: {{ request()->get('search') }}</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<section class="room_list_area">
        <div class="container">
            <div class="row room_list_inner">
                @if (!empty($posts))

                    <div class="search-info">
                        <div class="col-md-8 text-info">
                            <h2 class="">Pesquisando por: <span>{{ request()->get('search') }}</span></h2>
                        </div>
                        <div class="col-md-4 counter-info">
                            <h5><span>{{ count($posts) }}</span> {{ count($posts) > 1 ? 'notícias encontradas' : 'notícia encontrada'  }}</h5>
                        </div>
                    </div>

                    @foreach ($posts as $post)
                    <div class="room_list_item">
                        <div class="col-md-4">
                            <a href="{{ route('nav.post', ['url' => $post->url]) }}" class="room_img">
                                <img src="{{ asset('content/' . $post->id . "/" . $post->image) }}" alt="">
                            </a>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="room_list_l_text">
                                        <a href="{{ route('nav.post', ['url' => $post->url]) }}">
                                            <h4>{{ $post->title }}</h4>
                                        </a>
                                        <ul><li><a href="{{ route('nav.post', ['url' => $post->url]) }}">{{ $post->created_at->format('d/m/Y') }}</a></li></ul>
                                        <p>{!! $post->short_description !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="room_price_box">
                                        <a class="book_now_btn" href="{{ route('nav.post', ['url' => $post->url]) }}">Continue lendo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                <h1>Não foram encontrado posts para sua busca</h1>
                <img src="{{ asset('images/') }}" alt="">
                <aside class="r_widget search_widget">
                    <div class="input-group">
                        <form action="{{ route('nav.search') }}" method="GET">
                            <span class="input-group-btn">
                                <input name="search" type="text" class="form-control" placeholder="O que você procura?"
                                    required>
                                <button class="btn btn-default" type="submit"><i class="icon icon-Search"></i></button>
                            </span>
                        </form>
                    </div>
                </aside>
                @endif

            </div>
        </div>
    </section>






@endsection
